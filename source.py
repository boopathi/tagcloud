
twt = {};

twt['fredwilson']=[(u'mbamondays', 50), (u'ows', 50), (u'turntablefm', 40), (u'screwcable', 30), (u'coffeeshopboycott', 20), (u'dontbreaktheinternet', 20), (u'stopsopa', 20), (u'open', 20), (u'six', 10), (u'nyuef', 10), (u'greatestgiftever', 10), (u'stopcensorship:', 10), (u'occupy', 10), (u'nyc', 8), (u'tv', 5), (u'knicks', 5), (u'always', 4), (u'tech', 4)]

twt['ladygaga']=[(u'1', 150), (u'monsters', 62), (u'bornthisway', 32), (u'goblue', 30), (u'giantsgame', 30), (u'fashion', 29), (u'x', 27), (u'monster', 26), (u'gaga', 26), (u'performance', 24), (u'miss', 23), (u'marry', 23), (u'dadt', 21), (u'gagaforhaiti', 20), (u'weloveclarence?', 20), (u'gagatwitterqueen', 20), (u'buylife', 20), (u'1!', 20)]

twt['aplusk']=[(u'twoandahalfmen', 260), (u'footballforgood', 60), (u'nfl', 43), (u'investor', 41), (u'bears', 31), (u'stevejobs', 30), (u'tca', 30), (u'hut', 23), (u'twoandahalfmen?', 20), (u'brad', 20), (u'i', 20), (u'thm', 20), (u"punk'd", 20), (u'getamen', 20), (u'up', 20), (u'fashion', 12), (u'busted', 12), (u'christmas', 12)]

twt['cristiano']=[(u'worldcup', 170), (u'por', 145), (u'crpenaltygame', 90), (u'madrid', 57), (u'de', 54), (u'todosconmartins', 50), (u'hala', 43), (u'cr7freestyle', 40), (u'halamadrid', 40), (u'vamosajudarogustavo', 40), (u'testedtothelimit', 40), (u'el', 38), (u'portugal', 37), (u'crcontest', 30), (u'la', 30), (u'cr7flashtrial', 30), (u'que', 28), (u'fans', 27)]

twt['BARACKOBAMA']=[(u'obama2012', 840), (u'obama', 182), (u'debatewatch', 180), (u'wecantwait', 130), (u'dinnerwithbarack', 120), (u'passthebill', 110), (u'bo50', 100), (u'ff', 80), (u'progress', 70), (u'jobs', 62), (u'obama2012.', 60), (u'dinnerwithbarack?', 60), (u'dinnerwithbarack:', 60), (u'jobsnow', 60), (u'dadt', 60), (u'compromise', 52), (u'veteranscantwait', 50), (u'american', 47)]

import json

print "tweets = {\n";
for i in twt:
	print i + ":" +json.dumps(twt[i]) + ",\n"
print "}";
